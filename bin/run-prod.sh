docker stop node-api
docker rm node-api -f

rm .env
touch .env
echo "ENVIRONMENT=production" >> .env

docker run --name node-api \
    -p 3000:3000 node-api \
    sh -c "npm ci && npm run build && node dist/bundle.js"