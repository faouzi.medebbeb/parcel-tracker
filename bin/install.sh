npm i

npm run prepare
npx husky add .husky/pre-commit "npx lint-staged"

touch .env

echo ENVIRONMENT=development >> .env
echo MONGODB_DB_CONNECTION_URL= >> .env
echo MONGODB_DB_USER_NAME= >> .env
echo MONGODB_DB_PASSWORD= >> .env
echo WEATHER_API_KEY= >> .env
echo WEATHER_API_URL= >> .env