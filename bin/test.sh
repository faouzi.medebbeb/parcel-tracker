docker stop node-api
docker rm node-api -f

docker run -p 127.0.0.1:3000:3000 \
    --name node-api \
    --mount type=bind,src="$(pwd)",target=/app node-api \
    sh -c "npm test"