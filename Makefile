install:
	bash bin/install.sh

build:
	bash bin/build-image.sh

watch:
	bash bin/watch.sh

run-dev:
	bash bin/run-dev.sh

test-unit:
	bash bin/test.sh
	