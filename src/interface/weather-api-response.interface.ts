export interface WeatherApiResponseBody {
  data: Array<{ city_name: string; app_temp: number }>;
}
