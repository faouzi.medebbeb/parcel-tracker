export interface Shipment {
    trackingNumber: string
    carrier: string,
    senderAddress: string,
    receiverAddress: number,
    articleName: string,
    articleQuantity: number,
    articlePrice: number,
    status: string
}