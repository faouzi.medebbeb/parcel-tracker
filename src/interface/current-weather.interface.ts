export interface CurrentWeather {
  city: string;
  temperature: number;
}
