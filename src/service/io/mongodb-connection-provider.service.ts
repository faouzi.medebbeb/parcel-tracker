import { connect } from "mongoose";
import { ENV_VARIABLES } from "../../env";

export class MongoDBConnectionProvider {
  static async establishConnection() {
    try {
      return connect(
        `mongodb+srv://${ENV_VARIABLES.mongoDBUserName}:${ENV_VARIABLES.mongoDBPassword}@cluster-0.zy2o8se.mongodb.net/`,
        {
          dbName: "parcel-tracker",
        },
      );
    } catch (e) {
      // Report it to a monitoring tool
      // Make another connection attempt to the same DB, if still fails then try a connection to a backup/replica DB
      return null;
    }
  }
}
