import { ShipmentModel } from "../model/shipment.model";
import { MongoDBConnectionProvider } from "./io/mongodb-connection-provider.service";

export class ShipmentService {
  static async findAll() {
    const connection = await MongoDBConnectionProvider.establishConnection();

    if (!connection) {
      return null;
    }

    const entries = await ShipmentModel.find();

    return entries;
  }

  static async findBy(carrierId: string, trackingNumber: string) {
    const connection = await MongoDBConnectionProvider.establishConnection();

    if (!connection) {
      return null;
    }

    const carrierFilter = carrierId ? { carrier: carrierId } : {};
    const trackingNumberFilter = trackingNumber
      ? {
          trackingNumber,
        }
      : {};

    const finalFilter = { ...carrierFilter, ...trackingNumberFilter };

    const entries = await ShipmentModel.find(finalFilter);

    return entries;
  }
}
