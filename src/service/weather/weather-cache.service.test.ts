import { MongoDBConnectionProvider } from "../io/mongodb-connection-provider.service";
import { WeatherCacheService } from "./weather-cache.service";
import { CurrentWeatherModel } from "../../model/weather.model";
import { WEATHER_CACHE_TIME } from "../../constant/weather-cache-time.constant";

jest.mock("../io/mongodb-connection-provider.service");
jest.mock("../../model/weather.model");

describe("WeatherCacheService", () => {
  describe("getLatestWeatherEntryFor", () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    test("should return null if it could not establish a connection with the DB", async () => {
      // Arrange
      const establishConnectionSpy = jest
        .spyOn(MongoDBConnectionProvider, "establishConnection")
        .mockRejectedValueOnce(null);

      // Act
      const ret = await WeatherCacheService.getLatestWeatherEntryFor("", "");

      // Assert
      expect(establishConnectionSpy).toHaveBeenCalledTimes(1);
      expect(ret).toBeNull();
    });

    test("should return null if the connection return value is falsy", async () => {
      // Arrange
      const establishConnectionSpy = jest
        .spyOn(MongoDBConnectionProvider, "establishConnection")
        .mockResolvedValueOnce(null);

      // Act
      const ret = await WeatherCacheService.getLatestWeatherEntryFor("", "");

      // Assert
      expect(establishConnectionSpy).toHaveBeenCalledTimes(1);
      expect(ret).toBeNull();
    });

    test("should return null if no cached value was found", async () => {
      // Arrange
      const country = "de";
      const postalCode = "81249";
      const establishConnectionSpy = jest
        .spyOn(MongoDBConnectionProvider, "establishConnection")
        .mockImplementationOnce(jest.fn().mockResolvedValueOnce({}));
      const findSpy = jest
        .spyOn(CurrentWeatherModel, "find")
        .mockImplementationOnce(jest.fn().mockResolvedValueOnce([]));

      // Act
      const ret = await WeatherCacheService.getLatestWeatherEntryFor(
        country,
        postalCode,
      );

      // Assert
      expect(establishConnectionSpy).toHaveBeenCalledTimes(1);
      expect(findSpy).toHaveBeenCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({ country, postalCode });
      expect(ret).toBeNull();
    });

    test("should return null if the cached value is older than 2 hours", async () => {
      // Arrange
      const now = new Date().getTime();
      /**
       * Below is a computation of now minus 2 hours ago and 5 minutes
       *
       * The 5 minutes more is to avoid tests flakiness
       */
      const twoHoursAgo = now - (WEATHER_CACHE_TIME + 5) * 60 * 1000;
      const country = "de";
      const postalCode = "81249";
      const establishConnectionSpy = jest
        .spyOn(MongoDBConnectionProvider, "establishConnection")
        .mockImplementationOnce(jest.fn().mockResolvedValueOnce({}));
      const findSpy = jest
        .spyOn(CurrentWeatherModel, "find")
        .mockImplementationOnce(
          jest.fn().mockResolvedValueOnce([
            {
              createdAt: new Date(twoHoursAgo),
            },
          ]),
        );

      // Act
      const ret = await WeatherCacheService.getLatestWeatherEntryFor(
        country,
        postalCode,
      );

      // Assert
      expect(establishConnectionSpy).toHaveBeenCalledTimes(1);
      expect(findSpy).toHaveBeenCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({ country, postalCode });
      expect(ret).toBeNull();
    });

    test("should return the cached value if it is not older than 2 hours ago", async () => {
      // Arrange
      const now = new Date();
      const country = "de";
      const postalCode = "81249";
      const establishConnectionSpy = jest
        .spyOn(MongoDBConnectionProvider, "establishConnection")
        .mockImplementationOnce(jest.fn().mockResolvedValueOnce({}));
      const findSpy = jest
        .spyOn(CurrentWeatherModel, "find")
        .mockImplementationOnce(
          jest.fn().mockResolvedValueOnce([
            {
              createdAt: now,
              city: "Munich",
              temperature: 10,
            },
          ]),
        );

      // Act
      const ret = await WeatherCacheService.getLatestWeatherEntryFor(
        country,
        postalCode,
      );

      // Assert
      expect(establishConnectionSpy).toHaveBeenCalledTimes(1);
      expect(findSpy).toHaveBeenCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({ country, postalCode });
      expect(ret).toStrictEqual({ city: "Munich", temperature: 10 });
    });
  });

  describe("saveLatestWeatherEntryFor", () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    test("should return null if it could not establish a connection with the DB", async () => {
      // Arrange
      const establishConnectionSpy = jest
        .spyOn(MongoDBConnectionProvider, "establishConnection")
        .mockRejectedValueOnce(null);

      // Act
      const ret = await WeatherCacheService.saveLatestWeatherEntryFor({
        city: "",
        country: "",
        postalCode: "",
        temperature: 0,
      });

      // Assert
      expect(establishConnectionSpy).toHaveBeenCalledTimes(1);
      expect(ret).toBeNull();
    });

    test("should delete the existing cache entry before creating a new one and return it", async () => {
      // Arrange
      const establishConnectionSpy = jest
        .spyOn(MongoDBConnectionProvider, "establishConnection")
        .mockImplementationOnce(jest.fn().mockResolvedValueOnce({}));
      const deleteSpy = jest
        .spyOn(CurrentWeatherModel, "deleteOne")
        .mockResolvedValue(null);
      const createSpy = jest
        .spyOn(CurrentWeatherModel, "create")
        .mockImplementationOnce(
          jest.fn().mockResolvedValueOnce({
            city: "Munich",
            country: "de",
            postalCode: "81249",
            temperature: 0,
          }),
        );

      // Act
      const ret = await WeatherCacheService.saveLatestWeatherEntryFor({
        city: "Munich",
        country: "de",
        postalCode: "81249",
        temperature: 0,
      });

      // Assert
      expect(establishConnectionSpy).toHaveBeenCalledTimes(1);
      expect(deleteSpy).toHaveBeenCalledTimes(1);
      expect(deleteSpy).toHaveBeenCalledWith({
        country: "de",
        postalCode: "81249",
      });
      expect(createSpy).toHaveBeenCalledTimes(1);
      expect(createSpy).toHaveBeenCalledWith({
        city: "Munich",
        country: "de",
        postalCode: "81249",
        temperature: 0,
      });
      expect(ret).toStrictEqual({
        city: "Munich",
        country: "de",
        postalCode: "81249",
        temperature: 0,
      });
    });
  });
});
