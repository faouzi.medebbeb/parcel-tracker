import axios from "axios";
import { WeatherService } from "./weather.service";

jest.mock("axios");

describe("WeatherService", () => {
  describe("getCurrentWeather", () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    test("should return null if an error is caught", async () => {
      // Arrange
      const axiosGetSpy = jest.spyOn(axios, "get").mockRejectedValueOnce(null);

      // Act
      const ret = await WeatherService.getCurrentWeather("", "");

      // Assert
      expect(axiosGetSpy).toHaveBeenCalledTimes(1);
      expect(ret).toBeNull();
    });

    test("should return null if the data array from the weather api is empty", async () => {
      // Arrange
      const axiosGetSpy = jest.spyOn(axios, "get").mockImplementationOnce(
        jest.fn().mockResolvedValueOnce({
          data: { data: [] },
        }),
      );

      // Act
      const ret = await WeatherService.getCurrentWeather("", "");

      // Assert
      expect(axiosGetSpy).toHaveBeenCalledTimes(1);
      expect(ret).toBeNull();
    });

    test("should return a valid temperature and city data based on the country and the postal code", async () => {
      // Arrange
      const axiosGetSpy = jest.spyOn(axios, "get").mockImplementationOnce(
        jest.fn().mockResolvedValueOnce({
          data: {
            data: [
              {
                city_name: "Munich",
                app_temp: 10,
              },
            ],
          },
        }),
      );

      // Act
      const ret = await WeatherService.getCurrentWeather("", "");

      // Assert
      expect(axiosGetSpy).toHaveBeenCalledTimes(1);
      expect(ret).toStrictEqual({ city: "Munich", temperature: 10 });
    });
  });
});
