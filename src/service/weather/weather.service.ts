import axios from "axios";
import { ENV_VARIABLES } from "../../env";
import { WeatherApiResponseBody } from "../../interface/weather-api-response.interface";
import { CurrentWeather } from "../../interface/current-weather.interface";

export class WeatherService {
  static async getCurrentWeather(
    country: string,
    postalCode: string,
  ): Promise<CurrentWeather> {
    try {
      const {
        data: { data },
      } = await axios.get<WeatherApiResponseBody>(
        `${ENV_VARIABLES.weatherApiUrl}/current?postal_code=${postalCode}&country=${country}&key=${ENV_VARIABLES.weatherApiKey}`,
      );

      if (!data.length) {
        // Being here means that we could retrieve data for the given input
        return null;
      }

      const { city_name: city, app_temp: temperature } = data[0];

      return { city, temperature };
    } catch (e) {
      // Report it to a monitoring tool
      // Make another attempt, if still fails then try an alternative API for more fault tolerance
      return null;
    }
  }
}
