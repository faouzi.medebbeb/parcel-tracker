import { WEATHER_CACHE_TIME } from "../../constant/weather-cache-time.constant";
import { CurrentWeather } from "../../interface/current-weather.interface";
import { CurrentWeatherModel } from "../../model/weather.model";
import { MongoDBConnectionProvider } from "../io/mongodb-connection-provider.service";

export class WeatherCacheService {
  static async getLatestWeatherEntryFor(
    country: string,
    postalCode: string,
  ): Promise<CurrentWeather> {
    try {
      const connection = await MongoDBConnectionProvider.establishConnection();

      if (!connection) {
        return null;
      }

      const entries = await CurrentWeatherModel.find({
        country,
        postalCode,
      });

      if (!entries.length) {
        // Being here means that we haven't had a cached value yet
        return null;
      }

      const cacheEntry = entries[0];
      const now = new Date().getTime();
      const cacheEntryCreationTime = new Date(cacheEntry.createdAt).getTime();

      if ((now - cacheEntryCreationTime) / 1000 / 60 > WEATHER_CACHE_TIME) {
        // Being here means that the cache is older than 2 hours, hence we return null
        return null;
      }

      return {
        temperature: cacheEntry.temperature,
        city: cacheEntry.city,
      };
    } catch (e) {
      // Report it to a monitoring tool
      // Make another attempt, if still fails then try another backup/replica DB
      return null;
    }
  }

  static async saveLatestWeatherEntryFor({
    country,
    postalCode,
    city,
    temperature,
  }: {
    country: string;
    postalCode: string;
    city: string;
    temperature: number;
  }) {
    try {
      const connection = await MongoDBConnectionProvider.establishConnection();

      if (!connection) {
        return null;
      }

      // First clear the existing entry
      await CurrentWeatherModel.deleteOne({
        country,
        postalCode,
      });

      const createdDocument = await CurrentWeatherModel.create({
        country,
        postalCode,
        city,
        temperature,
      });

      return createdDocument;
    } catch (e) {
      // Report it to a monitoring tool
      // Make another attempt, if still fails then try another backup/replica DB
      return null;
    }
  }
}
