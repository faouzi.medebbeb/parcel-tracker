import "dotenv/config";

export const ENV_VARIABLES = {
  isDevelopment: process.env.ENVIRONMENT === "development",
  mongoDBConnectionUrl: process.env.MONGODB_DB_CONNECTION_URL,
  mongoDBUserName: process.env.MONGODB_DB_USER_NAME,
  mongoDBPassword: process.env.MONGODB_DB_PASSWORD,
  weatherApiKey: process.env.WEATHER_API_KEY,
  weatherApiUrl: process.env.WEATHER_API_URL
};
