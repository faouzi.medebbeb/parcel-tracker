import { Request, Response } from "express";
import { WeatherCacheService } from "../service/weather/weather-cache.service";
import { WeatherRouteHandler } from "./weather.handler";
import { WeatherService } from "../service/weather/weather.service";

jest.mock("../service/weather/weather-cache.service");

describe("WeatherRouteHandler", () => {
  describe("getCurrentWeather", () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    test("should return the value found in the cache", async () => {
      // Arrange
      const expectedReturnValue = {
        data: [{ city: "Munich", temperature: 0 }],
      };
      const requestParams = {
        country: "DE",
        postalCode: "81249",
      };
      const statusMock = jest.fn();
      const sendMock = jest.fn().mockReturnValueOnce({ status: statusMock });
      const getLatestWeatherEntryForSpy = jest
        .spyOn(WeatherCacheService, "getLatestWeatherEntryFor")
        .mockResolvedValueOnce({
          city: "Munich",
          temperature: 0,
        });

      // Act
      await WeatherRouteHandler.getCurrentWeather(
        {
          params: requestParams,
        } as unknown as Request,
        {
          send: sendMock,
        } as unknown as Response,
      );

      // Assert
      expect(getLatestWeatherEntryForSpy).toHaveBeenCalledTimes(1);
      expect(getLatestWeatherEntryForSpy).toHaveBeenCalledWith(
        requestParams.country.toLowerCase(),
        requestParams.postalCode,
      );
      expect(sendMock).toHaveBeenCalledTimes(1);
      expect(sendMock).toHaveBeenCalledWith(expectedReturnValue);
      expect(statusMock).toHaveBeenCalledWith(200);
    });

    test("should call the weather API, save the returned value in the cache and return it", async () => {
      // Arrange
      const requestParams = {
        country: "DE",
        postalCode: "81249",
      };
      const expectedReturnValue = {
        data: [{ city: "Munich", temperature: 0 }],
      };
      const statusMock = jest.fn();
      const sendMock = jest.fn().mockReturnValueOnce({ status: statusMock });
      const getLatestWeatherEntryForSpy = jest
        .spyOn(WeatherCacheService, "getLatestWeatherEntryFor")
        .mockResolvedValueOnce(null);
      const getCurrentWeatherSpy = jest
        .spyOn(WeatherService, "getCurrentWeather")
        .mockResolvedValueOnce({ city: "Munich", temperature: 0 });
      const saveLatestWeatherEntryForSpy = jest
        .spyOn(WeatherCacheService, "saveLatestWeatherEntryFor")
        .mockResolvedValueOnce(undefined);

      // Act
      await WeatherRouteHandler.getCurrentWeather(
        {
          params: requestParams,
        } as unknown as Request,
        {
          send: sendMock,
        } as unknown as Response,
      );

      // Assert
      expect(getLatestWeatherEntryForSpy).toHaveBeenCalledTimes(1);
      expect(getLatestWeatherEntryForSpy).toHaveBeenCalledWith(
        requestParams.country.toLowerCase(),
        requestParams.postalCode,
      );
      expect(getCurrentWeatherSpy).toHaveBeenCalledTimes(1);
      expect(saveLatestWeatherEntryForSpy).toHaveBeenCalledTimes(1);
      expect(saveLatestWeatherEntryForSpy).toHaveBeenCalledWith({
        city: "Munich",
        country: "de",
        postalCode: "81249",
        temperature: 0,
      });
      expect(sendMock).toHaveBeenCalledTimes(1);
      expect(sendMock).toHaveBeenCalledWith(expectedReturnValue);
      expect(statusMock).toHaveBeenCalledWith(200);
    });
  });
});
