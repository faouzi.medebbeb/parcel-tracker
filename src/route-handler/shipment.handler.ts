import { Request, Response } from "express";
import { ShipmentService } from "../service/shipment.service";

export class ShipmentRouteHandler {
  static async getAll(_: Request, res: Response) {
    const shipments = await ShipmentService.findAll();

    if (!shipments.length) {
      res
        .send({
          errors: [
            {
              description: "There seem to be no shipments to view",
            },
          ],
        })
        .status(404);
      return;
    }

    res
      .send({
        data: shipments,
      })
      .status(200);
  }

  static async getByTrackingNumber(req: Request, res: Response) {
    const { trackingNumber } = req.params;
    const normalizedTrackingNumber = trackingNumber.toLowerCase();

    const shipments = await ShipmentService.findBy(
      null,
      normalizedTrackingNumber,
    );

    if (!shipments.length) {
      res
        .send({
          errors: [
            {
              description:
                "Could not find shipments for the given tracking number",
            },
          ],
        })
        .status(404);
      return;
    }

    res
      .send({
        data: shipments,
      })
      .status(200);
  }
}
