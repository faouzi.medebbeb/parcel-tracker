import { Request, Response } from "express";
import { WeatherService } from "../service/weather/weather.service";
import { WeatherCacheService } from "../service/weather/weather-cache.service";
import { CurrentWeather } from "../interface/current-weather.interface";

export class WeatherRouteHandler {
  static async getCurrentWeather(req: Request, res: Response) {
    const { country, postalCode } = req.params;
    const normalizedCountry = country.toLowerCase();

    const currentWeatherFromCache: CurrentWeather | null =
      await WeatherCacheService.getLatestWeatherEntryFor(
        normalizedCountry,
        postalCode,
      );

    if (currentWeatherFromCache) {
      res.send({ data: [currentWeatherFromCache] }).status(200);
      return;
    }

    /**
     * Being here means that we either don't have a cached value yet or that
     * we have an expired cache
     */
    const currentWeatherFromWeatherApi = await WeatherService.getCurrentWeather(
      normalizedCountry,
      postalCode,
    );

    if (currentWeatherFromWeatherApi) {
      await WeatherCacheService.saveLatestWeatherEntryFor({
        ...currentWeatherFromWeatherApi,
        country: normalizedCountry,
        postalCode,
      });

      res.send({ data: [currentWeatherFromWeatherApi] }).status(200);
      return;
    }

    res
      .send({
        errors: [
          { description: "Could not find weather data for the given input" },
        ],
      })
      .status(404);
  }
}
