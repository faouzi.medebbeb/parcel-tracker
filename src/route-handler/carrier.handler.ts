import { Request, Response } from "express";
import { ShipmentService } from "../service/shipment.service";

export class CarrierRouteHandler {
  static async getShipments(req: Request, res: Response) {
    const { carrierId } = req.params;
    const normalizedCarrierId = carrierId.toLocaleLowerCase();

    const shipments = await ShipmentService.findBy(normalizedCarrierId, null);

    if (!shipments.length) {
      res
        .send({
          errors: [
            {
              description: "Could not find shipments for the given carrier",
            },
          ],
        })
        .status(404);
      return;
    }

    res
      .send({
        data: shipments,
      })
      .status(200);
  }

  static async getShipment(req: Request, res: Response) {
    const { carrierId, trackingNumber } = req.params;
    const normalizedCarrierId = carrierId.toLocaleLowerCase();
    const normalizedTrackingNumber = trackingNumber.toLocaleLowerCase();

    const shipments = await ShipmentService.findBy(
      normalizedCarrierId,
      normalizedTrackingNumber,
    );

    if (!shipments.length) {
      res
        .send({
          errors: [
            {
              description:
                "Could not find a shipment for the given carrier and tracking id",
            },
          ],
        })
        .status(404);
      return;
    }

    res
      .send({
        data: shipments,
      })
      .status(200);
  }
}
