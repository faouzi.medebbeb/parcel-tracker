import { Router } from "express";
import { CarrierRouteHandler } from "../route-handler/carrier.handler";

const carrierRoutes = Router();

carrierRoutes.get("/:carrierId/shipment", CarrierRouteHandler.getShipments);
carrierRoutes.get(
  "/:carrierId/shipment/:trackingNumber",
  CarrierRouteHandler.getShipment,
);

export { carrierRoutes };
