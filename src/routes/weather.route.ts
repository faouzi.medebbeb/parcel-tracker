import { Router } from "express";
import { WeatherRouteHandler } from "../route-handler/weather.handler";

const weatherRoutes = Router();

weatherRoutes.get(
  "/:country/:postalCode",
  WeatherRouteHandler.getCurrentWeather,
);

export { weatherRoutes };
