import { Router } from "express";
import { ShipmentRouteHandler } from "../route-handler/shipment.handler";

const shipmentRoutes = Router();

shipmentRoutes.get("/", ShipmentRouteHandler.getAll);
shipmentRoutes.get(
  "/:trackingNumber",
  ShipmentRouteHandler.getByTrackingNumber,
);

export { shipmentRoutes };
