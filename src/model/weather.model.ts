import mongoose from "mongoose";

const schema = new mongoose.Schema<{
    country: string
    city: string,
    postalCode: string,
    temperature: number,
    createdAt: string
}>({ country: String, city: String, postalCode: String, temperature: Number }, {
    collection: 'weather',
    timestamps: true
});

const CurrentWeatherModel = mongoose.model('CurrentWeatherModel', schema);

export { CurrentWeatherModel }