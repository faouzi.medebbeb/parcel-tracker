import mongoose from "mongoose";
import { Shipment } from "../interface/shipment.interface";

const schema = new mongoose.Schema<Shipment>({ 
    trackingNumber: String,
    carrier: String,
    senderAddress: String,
    receiverAddress: Number,
    articleName: String,
    articleQuantity: Number,
    articlePrice: Number,
    status: String
 }, {
    collection: 'shipment',
    timestamps: true
});

const ShipmentModel = mongoose.model('Shipment', schema);

export { ShipmentModel }