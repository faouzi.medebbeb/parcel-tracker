import express, { Request, Response } from "express";
import { weatherRoutes } from "./routes/weather.route";
import { carrierRoutes } from "./routes/carrier.route";
import { shipmentRoutes } from "./routes/shipment.route";

const app = express();
const port = 3000;

app.get("/health", (_: Request, res: Response) => {
  res.send("200 OK");
});

app.use("/weather", weatherRoutes);
app.use("/carrier", carrierRoutes);
app.use("/shipment", shipmentRoutes);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
