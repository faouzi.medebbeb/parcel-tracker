const path = require('path')
require('dotenv/config')

module.exports = {
  target: 'node',
  mode: process.env.ENVIRONMENT,
  watch: process.env.ENVIRONMENT === 'development',
  entry: './src/index.ts',
  module: {
    rules: [
      {
        test: /\.ts?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  devServer: {
    compress: true,
    port: 3000,
  },
};
